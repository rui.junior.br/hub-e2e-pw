import { Page, Locator} from '@playwright/test';

export class Flua{

    readonly page: Page;
    readonly inputCityName: Locator;
    readonly selectCity: Locator;
    readonly btnCloseCookies: Locator;
    readonly btnSelectOffer: Locator;
    readonly inputAmount: Locator;
    readonly btnNext: Locator;
    readonly inputFullName: Locator;
    readonly inputEmail: Locator;
    readonly inputDocument: Locator;
    readonly inputPhoneNumber: Locator;
    readonly openDealerModal: Locator;

    constructor(page){
        this.page = page;
        this.inputCityName = page.locator('input[name="geolocation-selector"]');
        this.selectCity = page.locator('ul.location-list > li > p');
        this.btnCloseCookies = page.getByRole('link', { name: 'Fechar' });
        this.btnSelectOffer = page.locator(`//button[text()[contains(.,'EU QUERO ESTE')]]`);
        this.inputAmount = page.getByRole('textbox'); //Campo de preenchimento da entrada, precisa melhorar esse nome depois.
        this.btnNext = page.getByRole('link', { name: 'Continuar' }).click();
        this.inputFullName = page.locator("//input[@placeholder='Digite seu nome completo']");
        this.inputEmail = page.locator("//input[@placeholder='Digite seu e-mail']");
        this.inputDocument =  page.locator("//input[@placeholder='___.___.___-__']");
        this.inputPhoneNumber = page.locator("//input[@placeholder='(__) _____-____']");
        this.openDealerModal = page.getByText('Escolha aqui a sua concessionária');
    }

      async selectOffer(index) {
        await this.btnCloseCookies.click();
        await this.btnSelectOffer.nth(index).click();
      }
    
      async selectContractOption(amount) {
        await this.inputAmount.fill(amount);
      }
    
      async fillLeadInformation(name, email, document, phoneNumber) {
        await this.inputFullName.fill(name);
        await this.inputEmail.fill(email);
        await this.inputDocument.fill(document);
        await this.inputPhoneNumber.fill(phoneNumber);
      }
    
      async selectDealer(cssName) {
        await this.openDealerModal.click();
        await this.page.getByText(cssName).click();
      }

}