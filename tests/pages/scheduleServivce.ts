import { Page, Locator} from '@playwright/test';

export class ScheduleServices{

    readonly page: Page;
    readonly selectYear: Locator
    readonly selectedYear: Locator;
    readonly selectedModel: Locator;
    readonly selectModel: Locator;
    readonly plate: Locator;
    readonly milleage: Locator;
    readonly serviceSelected: Locator;
    readonly btnContinueScheduled: Locator;
    readonly btnFillUserData: Locator;
    readonly inputName: Locator;
    readonly inputEmail: Locator;
    readonly cpf: Locator;
    readonly phone: Locator;
    readonly btnConcluedScheduled: Locator;
    readonly btnConfirmedScheduled: Locator;
    readonly selectedDay: Locator;

    constructor(page){
        this.page = page;
        this.selectYear = page.locator("//span[text()[contains(.,'Selecione o ano')]]");
        this.selectedYear = page.locator("//li[text()[contains(.,'2023')]]");
        this.selectModel = page.locator("//span[text()[contains(.,'Selecione o modelo')]]");
        this.selectedModel = page.locator("//li[text()[contains(.,'ARGO 1.0 FLEX 4P 2023')]]");
        this.plate = page.locator("//input[@placeholder='ex.: XYZ0000']");
        this.milleage = page.locator("//input[@placeholder='Selecione a quilometragem']");
        this.serviceSelected = page.locator("//div[text()[contains(.,'Troca de oleo 1')]]");
        this.btnContinueScheduled = page.locator("//span[text()[contains(.,'IR PARA SELEÇÃO DE DATA E HORÁRIO')]]");
        this.btnFillUserData = page.locator("//span[text()[contains(.,'INFORME SEUS DADOS')]]");
        this.inputName = page.locator("//input[@placeholder='Nome Completo']");
        this.inputEmail = page.locator("//input[@placeholder='E-mail']");
        this.cpf = page.locator("//input[@placeholder='000.000.000-00']");
        this.phone = page.locator("//input[@placeholder='ex: (11) 99999-9999']");
        this.btnConcluedScheduled = page.locator("//span[text()[contains(.,'CONCLUIR SOLICITAÇÃO')]]");
        this.btnConfirmedScheduled = page.locator("//span[text()[contains(.,'CONFIRMAR DADOS')]]");
        this.selectedDay = page.locator("//span[@class='cell day'][3]");
    }
    
    async fillFieldYear(){
      await this.selectYear.first().click();
      await this.selectedYear.click();
    }

    async fillFieldModel(){
      await this.selectModel.first().click();
      await this.selectedModel.click();
    }

    async fillPlateAndMilleage(plate, milleage){
      await this.plate.click();
      await this.plate.fill(plate);
      await this.milleage.click();
      await this.milleage.fill(milleage);
    }
    
    async setService(){
      await this.serviceSelected.click();
    }

    async continueToData(){
      await this.btnContinueScheduled.click();
    }

    async fillHour(){
      await this.selectedDay.click();
      await this.btnFillUserData.click();
    }

    async fillDataUser(name, email, cpf, phone){
      await this.inputName.click();
      await this.inputName.fill(name);
      await this.inputEmail.click();
      await this.inputEmail.fill(email);
      await this.cpf.click();
      await this.cpf.fill(cpf);
      await this.phone.click();
      await this.phone.fill(phone);
      await this.btnConfirmedScheduled.click();
    }

    async concluedScheduled(){
      await this.btnConcluedScheduled.click();
    }
}