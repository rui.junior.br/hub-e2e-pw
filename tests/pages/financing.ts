import { Locator, Page } from "@playwright/test"

export class FinancingPage{

    readonly page: Page;
    readonly btnSimulateInstallments: Locator;
    readonly inputName: Locator;
    readonly inputEmail: Locator;
    readonly inputCPF: Locator;
    readonly inputPhone: Locator;
    readonly btnSendLeadSimulateRegular: Locator;
    readonly selectNext: Locator;
    readonly selectRegular: Locator;
    readonly sendProposal: Locator;
    readonly btnSendLeadNext: Locator;

    constructor(page){
        this.page = page
        this.btnSimulateInstallments = page.locator("//button[text()[contains(.,'Simule as parcelas')]]");
        this.inputName = page.locator("//input[@type='text']");
        this.inputEmail = page.locator("//input[@type='email']");
        this.inputCPF = page.locator("//input[@class='hub-input-field cpf']");
        this.inputPhone = page.locator("//input[@class='hub-input-field phone']");
        this.btnSendLeadSimulateRegular =  page.locator("//span[text()[contains(.,'SIMULAR PARCELAS')]]");
        this.selectNext = page.locator("//span[text()[contains(.,'NEXT JEEP')]]");
        this.selectRegular = page.locator("//span[text()[contains(.,'FINANCIAMENTO')]]");
        this.sendProposal = page.locator("//span[text()[contains(.,'SOLICITE UMA PROPOSTA')]]");
        this.btnSendLeadNext = page.locator("//span[text()[contains(.,'ENVIAR')]]");
    }

    async financingNext(name, email, cpf, phone){
        await this.btnSimulateInstallments.click();
        await this.selectNext.nth(1).click();
        await this.sendProposal.click();
        await this.inputName.first().click();
        await this.inputName.first().fill(name);
        await this.inputEmail.first().click();
        await this.inputEmail.first().fill(email);
        await this.inputCPF.first().click();
        await this.inputCPF.first().fill(cpf);
        await this.inputPhone.first().click();
        await this.inputPhone.first().fill(phone);
    }

    async sendLeadNext(){
        await this.btnSendLeadNext.click();
    }

}