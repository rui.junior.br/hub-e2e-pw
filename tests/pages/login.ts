import { Page, Locator } from '@playwright/test';

export class Login {
    readonly page: Page;
    readonly singInEmail: Locator;
    readonly userName: Locator;
    readonly password: Locator;
    readonly login: Locator;
    readonly signIn: Locator;
    readonly name: Locator;

    constructor(page) {
        this.page = page;
        this.singInEmail = page.locator("//div[@class='pointer']");
        this.userName = page.locator("//input[@type='email']");
        this.password = page.locator("//input[@type='password']");
        this.name = page.locator("//input[@type='text']");
        this.login = page.locator("//span[text()[contains(.,'CONTINUAR')]]");
        this.signIn = page.locator('#header-component >> text=Entre ou Cadastre-se');
    }
    
      async userSignIn(user, pw) {
        await this.singInEmail.click();
        await this.userName.click();
        await this.userName.fill(user);
        await this.login.click();
        await this.password.click();
        await this.password.fill(pw);
        await this.login.click();
      }

      async createAccount(new_user, name, password){
        await this.singInEmail.click();
        await this.userName.click();
        await this.userName.fill(new_user);
        await this.login.click();
        await this.name.click();
        await this.name.fill(name);
        await this.name.press('Enter');
        await this.name.click();
        await this.password.fill(password);
        await this.password.click();
        await this.password.fill(password);
      }
}
