import { Page, Locator} from '@playwright/test';

export class NewCarShowcase {
    readonly page: Page;
    readonly title: Locator;
    readonly version: Locator;
    readonly btnProximo: Locator;
    readonly btnConcluir: Locator;
    readonly btnVamosNegociar: Locator;
    readonly v: Locator;
    readonly inputTextName: Locator;
    readonly inputTextEmail: Locator;
    readonly inputTextPhone: Locator;
    readonly inputTextCpf: Locator;
    readonly inputTextLocation: Locator;
    readonly selectedCity: Locator;
    readonly btnSendLead: Locator;
    readonly messageSuccess: Locator;
    readonly selectedDealer: Locator;


    constructor(page) {
        this.page = page;
        this.title = page.locator('text=Monte o seu RENEGADE');
        this.btnProximo = page.locator("//span[text()[contains(.,'próximo:')]]");
        this.btnConcluir = page.locator("//span[text()[contains(.,' Concluir')]]");
        this.btnVamosNegociar = page.locator("//span[text()[contains(.,' Vamos negociar ')]]"); 
        this.v = page.locator("//span[text()[contains(.,' SÓLIDAS')]]")
        this.inputTextName = page.locator("(//input[@type='text'])[1]");
        this.inputTextEmail = page.locator("(//input[@type='text'])[2]");
        this.inputTextPhone = page.locator("(//input[@type='text'])[3]");
        this.inputTextCpf = page.locator("(//input[@type='text'])[4]");
        this.inputTextLocation = page.locator("(//input[@type='text'])[5]");
        this.selectedCity = page.locator("//div[@class='pac-item']");
        this.btnSendLead = page.locator("//a[@class='hub-button hub-button']");
        this.messageSuccess = page.locator("//p[text()[contains(.,'A configuração do seu carro foi concluída!')]]");
        this.selectedDealer = page.locator("//label[@for='790578']");
      }

    async customizeYourCar(){
      await this.btnProximo.click();
      await this.btnProximo.click();
      await this.btnProximo.click();
      await this.btnProximo.click();
      await this.btnConcluir.click();
    }

    async sendLead(){
      await this.btnVamosNegociar.click();
      await this.inputTextName.first().fill('Amém Senhor');
      await this.inputTextEmail.first().fill('v@v.com');
      await this.inputTextPhone.first().fill('83991418873');
      await this.inputTextCpf.first().fill('73670678506');
      await this.inputTextLocation.first().fill('31050-610');
      await this.selectedCity.click();
      await this.selectedDealer.click();
      await this.btnSendLead.click();
    }
}

