import { Locator, Page } from "@playwright/test";

export class OfferPage{
    readonly page: Page;
    readonly inputName: Locator;
    readonly inputEmail: Locator;
    readonly inputCpf: Locator;
    readonly inputPhone: Locator;
    readonly inputCategory: Locator;
    readonly inputModel: Locator;
    readonly inputVersion: Locator;
    readonly dealerDropdown: Locator;
    readonly dealerSelector: Locator;
    readonly btnsendLeadOffer: Locator;

    constructor(page){
        this.page = page;
        this.inputName = page.locator("//input[@placeholder='Digite um nome válido']");
        this.inputEmail = page.locator("//input[@placeholder='Digite um e-mail válido']");
        this.inputCpf = page.locator("//input[@placeholder='Digite um documento']");
        this.inputPhone = page.locator("(//input[@class='hub-input-field'])[4]");
        this.inputCategory = page.locator("(//span[text()[contains(.,'Selecione uma opção')]])[1]");
        this.inputModel = page.locator("(//span[text()[contains(.,'Selecione o modelo')]])[1]");
        this.inputVersion = page.locator("(//span[text()[contains(.,'Selecione a versão')]])[1]");
        this.dealerDropdown = page.locator("(//input[@placeholder='Digite um nome válido'])[1]");
        this.btnsendLeadOffer = page.locator("//span[text()[contains(.,'ENVIAR')]]");
    }

    async fillUserData(name,email,cpf,phone,category,model,version){
        await this.inputName.click();
        await this.inputName.fill(name);
        await this.inputEmail.click();
        await this.inputEmail.fill(email);
        await this.inputCpf.click();
        await this.inputCpf.fill(cpf);
        await this.inputPhone.click();
        await this.inputPhone.fill(phone);
        await this.inputCategory.click();
        await this.page.locator(`(//li[text()[contains(.,"${category}")]])[1]`).click();
        await this.inputModel.click();
        await this.page.locator(`(//li[text()[contains(.,"${model}")]])[1]`).click();
        await this.inputVersion.click();
        await this.page.locator(`(//li[text()[contains(.,"${version}")]])[1]`).click();
    }

    async sendLeadOffer(){
        await this.btnsendLeadOffer.click();
    }
}