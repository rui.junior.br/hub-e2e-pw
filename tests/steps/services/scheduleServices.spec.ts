import { test, expect } from '@playwright/test';
import { MethodsCommons } from '../../utils/methodsCommons';
import { ScheduleServices } from '../../pages/scheduleServivce';


test.describe('Test Suite - Scheduled Services', () => {

    let scheduleServices;
    let commons;


    test.beforeEach(async ({ page }) => {

        scheduleServices = new ScheduleServices(page);
        commons = new MethodsCommons(page);


        await commons.loadToPage('/content/fiat/BR/pt/servicos/agendamento.html');
    });

    test('Scheduled service in webpage', async ({ page }) => {
        const messageSuccess = page.locator("//p[@class='confirm-popup-text']");

        await scheduleServices.fillFieldYear();
        await scheduleServices.fillFieldModel();
        await scheduleServices.fillPlateAndMilleage('RLF0966', '30000');
        await scheduleServices.setService()
        await commons.setDealer("31050-610", '918565');
        await scheduleServices.continueToData();
        await scheduleServices.fillHour();
        await scheduleServices.fillDataUser('Assis da Silva', 'v@v.com', '736.706.785-06', '89987651234');
        await scheduleServices.concluedScheduled();
        expect(messageSuccess).toHaveText("Em breve você vai receber no seu e-mail informações sobre a revisão agendada.");
    });

    test('Scheduled service without plate and milleage in webpage', async ({ page }) => {
        const messageFailedPlateNumber = page.locator("//span[text()[contains(.,'Digite a placa do carro')]]");
        const messageFailedMilleage = page.locator("//span[text()[contains(.,'Indique a quilometragem')]]");

        await scheduleServices.fillFieldYear();
        await scheduleServices.fillFieldModel();
        await scheduleServices.fillPlateAndMilleage('', '');
        await scheduleServices.setService()
        await commons.setDealer("31050-610", '918565');
        await scheduleServices.continueToData();
        expect(messageFailedPlateNumber).toHaveText("Digite a placa do carro");
        expect(messageFailedMilleage).toHaveText("Indique a quilometragem");
    });

    test('Scheduled service in webpage without data user', async ({ page }) => {
        const dataName = page.locator("//span[text()[contains(.,'Digite um nome válido')]]");
        const dataEmail = page.locator("//span[text()[contains(.,'Digite um e-mail válido')]]");
        const dataCpf = page.locator("//span[text()[contains(.,'CPF inválido')]]");
        const dataPhone = page.locator("//span[text()[contains(.,'Digite seu celular')]]");

        await scheduleServices.fillFieldYear();
        await scheduleServices.fillFieldModel();
        await scheduleServices.fillPlateAndMilleage('RLF0966', '30000');
        await scheduleServices.setService()
        await commons.setDealer("31050-610", '918565');
        await scheduleServices.continueToData();
        await scheduleServices.fillHour();
        await scheduleServices.fillDataUser('', '', '', '');
        expect(dataName).toHaveText("Digite um nome válido");
        expect(dataEmail).toHaveText("Digite um e-mail válido");
        expect(dataCpf).toHaveText("CPF inválido");
        expect(dataPhone).toHaveText("Digite seu celular");
    });

    test('Scheduled service in webpage with data user invalid', async ({ page }) => {
        const dataName = page.locator("//span[text()[contains(.,'Digite um nome válido')]]");
        const dataEmail = page.locator("//span[text()[contains(.,'Digite um e-mail válido')]]");
        const dataCpf = page.locator("//span[text()[contains(.,'CPF inválido')]]");
        const dataPhone = page.locator("//span[text()[contains(.,'Digite seu celular')]]");

        await scheduleServices.fillFieldYear();
        await scheduleServices.fillFieldModel();
        await scheduleServices.fillPlateAndMilleage('RLF0966', '30000');
        await scheduleServices.setService()
        await commons.setDealer("31050-610", '918565');
        await scheduleServices.continueToData();
        await scheduleServices.fillHour();
        await scheduleServices.fillDataUser('x', 'v.com', '12345678911', '1');
        expect(dataName).toHaveText("Digite um nome válido");
        expect(dataEmail).toHaveText("Digite um e-mail válido");
        expect(dataCpf).toHaveText("CPF inválido");
        expect(dataPhone).toHaveText("Digite seu celular");
    });
});