import { test, expect } from '@playwright/test';
import { MethodsCommons } from '../../utils/methodsCommons';
import { FinancingPage } from '../../pages/financing';


test.describe('Test Suite - Financing', () => {
  
  let financing;
  let commons;

  test.beforeEach(async ({ page }) => {

    financing = new FinancingPage(page);
    commons = new MethodsCommons(page);

    await commons.loadToPage('/content/jeep/BR/pt/products/611.html')
    await commons.setLocation("Belo Horizonte", "MG");
  });

  test('Financing next send lead with 30%', async ({ page }) => {
    await financing.financingNext('Assis da Silva', 'v@v.com', '736.706.785-06', '89987651234');
    await commons.setDealer('31050610','790578');
    await financing.sendLeadNext();
    expect(page.locator("//p[@class='confirm-popup-title']")).toHaveText('SUA PROPOSTA FOI ENVIADA!');
  });

  test('Financing next send lead without dealer code', async ({ page }) => {
    await financing.financingNext('Assis da Silva', 'v@v.com', '736.706.785-06', '89987651234');
    await financing.sendLeadNext();
    expect(page.locator("//span[text()[contains(.,'Selecione uma concessionária')]]")).toHaveText('Selecione uma concessionária');
  });

  test('Financing next send lead without cpf phone, name, email and dealer code', async ({ page }) => {
    await financing.financingNext('','', '', '(55) 55555-5555');
    await financing.sendLeadNext();
    expect(page.locator("//span[text()[contains(.,'Digite seu nome')]]")).toHaveText('Digite seu nome');
    expect(page.locator("//span[text()[contains(.,'Digite um email válido')]]")).toHaveText('Digite um email válido');
    expect(page.locator("//span[text()[contains(.,'CPF inválido')]]")).toHaveText('CPF inválido');
    expect(page.locator("//span[text()[contains(.,'Digite um celular válido')]]")).toHaveText('Digite um celular válido'); 
    expect(page.locator("//span[text()[contains(.,'Selecione uma concessionária')]]")).toHaveText('Selecione uma concessionária');
  });
});