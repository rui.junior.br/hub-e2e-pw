import { test, expect } from '@playwright/test';
import { MethodsCommons } from '../../utils/methodsCommons';
import { Flua } from '../../pages/flua';


test.describe('Test Suite - Flua', () => {
  
  let flua;
  let commons;

  test.beforeEach(async ({ page }) => {

    flua = new Flua(page);
    commons = new MethodsCommons(page);

    await commons.loadToPage('/content/flua/BR/pt/home/fiat.html')
    await commons.setLocation("São Paulo","SP");
  });

  test('Sending a successfully lead', async ({ page }) => {
    await flua.selectOffer(0);
    await flua.selectContractOption('R$ 7.500,00');
    await flua.fillLeadInformation('Josefa Candido', 'josefa.automacao01@mailinator.com', '182.029.300-90', '(01) 99999-8888');
    await flua.selectDealer('AMAZONAS-SUMARE');
    await page.getByRole('link', { name: 'ENVIAR' }).click();
    await expect(page.locator('.modal-title')).toHaveText('Sucesso!');
    await expect(page.locator('.message > p')).toContainText('Você está prestes a conhecer um novo jeito de ter um carro.');
  });

  test('Sending a lead without a fullName', async ({ page }) => {
    await flua.selectOffer(0);
    await flua.selectContractOption('R$ 7.500,00');
    await flua.fillLeadInformation('', 'josefa.automacao01@mailinator.com', '182.029.300-90', '(01) 99999-8888');
    await flua.selectDealer('AMAZONAS-SUMARE');
    await page.getByRole('link', { name: 'ENVIAR' }).click();
    await expect(page.locator('.error-container')).toContainText('Digite um nome válido');
    await expect(page.locator('.error-container')).toContainText('Digite pelo menos um sobrenome');
  });

});