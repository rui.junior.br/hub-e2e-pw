import { test, expect } from '@playwright/test';
import { MethodsCommons } from '../../utils/methodsCommons';
import { OfferPage } from '../../pages/offer';


test.describe('Test Suite - Ofertas', () => {
  
  let offer;
  let commons;

  test.beforeEach(async ({ page }) => {

    offer = new OfferPage(page);
    commons = new MethodsCommons(page);

    await commons.loadToPage('/content/fiat/BR/pt/ofertas.html')
    await commons.setLocation("Belo Horizonte","MG");
  });

  test('Sending a successfully lead ofertas get voucher', async ({ page }) => {
    const name = "Teste automatizado";
    await offer.fillUserData(name,"t@t.com","73670678506","88999999999","PESSOA FÍSICA","ARGO"," DRIVE 1.0 FLEX 4P 2020");
    await commons.setDealer('31050610', '913293');
    await offer.sendLeadOffer();
    expect(page.locator("//strong[@class='client-name']")).toHaveText(name);
  });

  test('Sending lead ofertas without dealer code', async ({ page }) => {
    await offer.fillUserData("Teste automatizado","t@t.com","73670678506","88999999999","PESSOA FÍSICA","ARGO"," DRIVE 1.0 FLEX 4P 2020");
    await offer.sendLeadOffer();
    expect(page.locator("(//span[text()[contains(.,'Selecione uma concessionária')]])[1]")).toHaveText('Selecione uma concessionária');
  });

  test('Sending lead ofertas without name, email, dealer code', async ({ page }) => {
    await offer.fillUserData("","","12345678999","","PESSOA FÍSICA","ARGO"," DRIVE 1.0 FLEX 4P 2020");
    await offer.sendLeadOffer();
    expect(page.locator("(//span[text()[contains(.,'Selecione uma concessionária')]])[1]")).toHaveText('Selecione uma concessionária');
    expect(page.locator("(//span[text()[contains(.,'Digite seu nome')]])[1]")).toHaveText('Digite seu nome');
    expect(page.locator("(//span[text()[contains(.,'Digite um e-mail válido')]])[1]")).toHaveText('Digite um e-mail válido');
    expect(page.locator("(//span[text()[contains(.,'Digite um CPF válido')]])[1]")).toHaveText('Digite um CPF válido');
    expect(page.locator("(//span[text()[contains(.,'Digite um telefone válido')]])[1]")).toHaveText('Digite um telefone válido');
  });

});