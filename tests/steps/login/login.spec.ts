import { test, expect } from '@playwright/test';
import { Login } from '../../pages/login';
import { MethodsCommons } from '../../utils/methodsCommons';


test.describe('Test Suite - Login', () => {
  
  let login;
  let homePage;
  let commons;

  const user = 'ac.fca.user1@avenuecode.com';
  const pw = 'Ac4success@2018#';
  const fistName = 'doroteia';

  test.beforeEach(async ({page}) => {
    // Será executado antes de cada teste
    login = new Login(page);
    commons = new MethodsCommons(page);

    await commons.loadToPage('/content/fiat/BR/pt/home.html') // Acessar site
  });

  test('Should login successfully', async ({ page }) => {
    await commons.singInLink();
    await login.userSignIn(user, pw); 
    await expect(page.locator('#header-component')).toContainText(fistName);
  });
});