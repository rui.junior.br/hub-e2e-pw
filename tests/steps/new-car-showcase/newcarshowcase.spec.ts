import { test, expect } from '@playwright/test';
import { NewCarShowcase } from '../../pages/newcarshowcase';
import { MethodsCommons } from '../../utils/methodsCommons';


test.describe('Test Suite - New CarShowCase', () => {
  
  let newcarshowcase;
  let commons;

  test.beforeEach(async ({ page }) => {

    newcarshowcase = new NewCarShowcase(page);
    commons = new MethodsCommons(page);

    await commons.loadToPage('/content/jeep/BR/pt/products/611.html')
    await commons.setLocation("Belo Horizonte", "MG");
  });

  test('Navigator from version, accessories and resume and send lead', async ({ page }) => {
    expect(newcarshowcase.title).toHaveText('Monte o seu RENEGADE');
    await newcarshowcase.customizeYourCar();
    await newcarshowcase.sendLead();
    expect(page.locator("//p[text()[contains(.,'A configuração do seu carro foi concluída!')]]")).toHaveText('A configuração do seu carro foi concluída!');
  });
});