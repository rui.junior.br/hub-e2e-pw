import { Page, Locator, expect } from '@playwright/test';

export class MethodsCommons {
    readonly page: Page;
    readonly signIn: Locator;
    readonly inputCity: Locator;
    readonly selectCity: Locator;
    readonly cep: Locator;
    readonly selectedCity: Locator;
    

    constructor(page: Page) {
        this.page = page;
        this.signIn = page.locator('#header-component >> text=Entre ou Cadastre-se');
        this.inputCity = page.locator('input[name="geolocation-selector"]');
        this.selectCity = page.locator('ul.location-list > li > p')
        this.cep = page.locator("//input[@placeholder='Informe seu CEP, endereço ou cidade']");
        this.selectedCity = page.locator("//div[@class='pac-item']");
    }

    async loadToPage(path){
        await this.page.goto(path);
    }

    async setLocation(cityName, stateCode) {
        await this.inputCity.isVisible();
        await this.inputCity.click();
        await this.inputCity.fill(cityName);
        await this.inputCity.press('Space');
        await this.inputCity.press('Enter');
        await this.selectCity.filter({ hasText: `${cityName} - ${stateCode}` }).click();
    }

    async setDealer(cep, dealerCode){
        await this.cep.click();
        await this.cep.fill(cep);
        await this.selectedCity.first().click();
        await this.page.locator(`//input[@id=${dealerCode}]`).click();
    }

    async clickLink(linkText){
        await this.page.locator(`text=${linkText}`).click();
    }

    async singInLink(){
        await this.signIn.click();
    }

    async tap(element){
        await this.page.locator(element).click();
    }
}